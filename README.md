# DOKKU INFRASTRUCTURE

This Ansible + Cloudformation bootstrap will let you create the basic skeleton of a Dokku instance in AWS.

### Requirements

The bootstrap was tested with the following components:
 - Ansible 2.4.3
 - boto 2.48.0
 - boto3 1.4.6
 - botcore 1.8.20
 - Jinja 1.2
 - Jinja 2.10
 - paramiko 2.3.1
 - PyYAML 3.12
 - Git


## DEFINE GENERAL INFRASTRUCTURE

### Project structure
```
.
├── ansible
│   ├── bootstrap.yaml    --> Ansible playbook
│   └── vars
│       └── env.yaml      --> Settings file
├── aws
│   └── cloudformation
│       ├── compute.yaml  --> AWS Cloudformation compute stack
│       └── vpc.yaml      --> AWS Cloudformation VPC stack
└── README.md
```

### Defining important variables

Adjust values under `./ansible/vars/env.yaml` as necessary:

```yaml
---
region: us-west-2
dc_type: dev
project: r3pi
vpc_cidr: 10.122.1.1/16

hosted_zone:
  id: Z8QRP6D5ULN61
  name: devoops.co

subnets:
  subnet_pub_az1: 10.122.1.1/24
  subnet_pub_az2: 10.122.2.1/24
  subnet_pvt_az2: 10.122.3.1/24
  subnet_pvt_az1: 10.122.4.1/24

bastion:
  instance_type: t2.nano
  launch_ssh_key: id_rsa

dokku:
  component: dokku
  instance_type: t2.small
  launch_ssh_key: id_rsa
  ip_whitelist: 0.0.0.0/0
  assign_public_ip: "false"
  public_key: "ssh-rsa ******"
  load_balancing:
    dns_entry: dokku.sample.com
    ssl_cert: "na"

jenkins:
  component: jenkins
  instance_type: t2.small
  launch_ssh_key: miguelpereira
  ip_whitelist: 0.0.0.0/0
  assign_public_ip: "false"
  public_key: "ssh-rsa ******"
  load_balancing:
    dns_entry: jenkins.sample.com
    ssl_cert: "na"
...
```

### Deploy infrastructure

Ensure your [BOTO](https://boto3.readthedocs.io/en/latest/) profile is configured and pointing to the AWS account where you want to create your infra. Then, run the following from a shell terminal:

```bash
https://gitlab.com/miguelpereira/dokku-infra.git
cd ./dokku-infra
ansible-playbook ./ansible/bootstrap.yaml --extra-vars @./ansible/vars/env.yaml
```

### Testing connectivity

If you used a default key:
```bash
ssh dokku@dokku.sample.com
```

Otherwise, you can specify the private key:
```bash
ssh dokku@dokku.sample.com -i ~/.ssh/other_private_key
```

If all goes well and the infrastructure's got the time to bootstrap, you'll see something like this:

```
Usage: dokku [--quiet|--trace|--rm-container|--rm|--force] COMMAND <app> [command-specific-options]

Primary help options, type "dokku COMMAND:help" for more details, or dokku help --all to see all commands.

Commands:

    apps             Manage Dokku apps
    certs            Manage Dokku apps SSL (TLS) certs
    checks           Manage zero-downtime settings
    config           Manages global and app-specific config vars
    docker-options   Pass options to Docker the various stages of an app
    domains          Manage vhost domains used by the Dokku proxy
    enter            Connect to a specific app container
    events           Show the last events (-t follows)
    help             Print the list of commands
    logs             Show the last logs for an application
    ls               Pretty listing of deployed applications and containers
    network          Manages network settings for an app
    nginx            Interact with Dokku's Nginx proxy
    proxy            Manage the proxy used by dokku on a per app
    ps               List processes running in app container(s)
    repo             Runs commands that interact with the app's repo
    run              Run a command in a new container using the current application image
    shell            Spawn dokku shell
    ssh-keys         Manage public ssh keys that are allowed to connect to Dokku
    storage          Mount local volume / directories inside containers
    tags             List all app image tags
    tar              Deploy applications via tarball instead of git
    trace            Enable dokku tracing
    url              Show the first URL for an application (compatibility)
    urls             Show all URLs for an application
    version          Print dokku's version
```

Indicating that the dokku instance has been launched successfully and is ready to go.

## SETUP / CONFIGURE DOKKU

The dokku instance will be configured automatically based on the values provided on `env.yaml`. We do this via the launch configuration user data.

Also, please note that we are shutting down the `web-installer` service almost immediately to avoid leaving the instance open to the world --though we have network level ACLs preventing the instance from being publicly available.

## DEPLOY `dokku-sample-app`

Clone the app repo locally

```bash
git clone git@gitlab.com:miguelpereira/dokku-sample-app.git
```

Create a _dokku remote_

```bash
cd ./dokku-sample-app
git remote add dokku dokku@dokku.sample.com:dokku-sample-app
```

Deploy the app

```bash
git push dokku master
```

## CI/CD VIA JENKINS
This project launches a containerized instance of Jenkins; its configuration falls outside of the scope of this excercise. However, the following are some of the high level areas that must be done on a fresh instance of Jenkins to integrate it w/GitLab:

### Plugins
- Stock Plugins
- Blue Ocean + Deps
- Gitlab
- Gitlab Logo
- Gitlab Merge Request Builder
- Gitlab Hook

### High level settings
- Setup GitLab API token on GitLab
- Configure the GitLab API token on Jenkins CI
- Add a pipeline job pointing to the repo hosting `dokku-sample-app`
  - Jenkins will present a public key; import this key on your GitLab project key -grant it read-only permissions

### Notes and links I found useful
- If you ever get an error --> [_app does not appear to be a git repository_](https://github.com/dokku/dokku/issues/1813)
- About [dokku key management](https://github.com/dokku/dokku/blob/master/docs/deployment/user-management.md)
- The ubiquitous [sample application](http://dokku.viewdocs.io/dokku~v0.11.5/deployment/application-deployment/) from the documentation
