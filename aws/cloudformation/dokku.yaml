AWSTemplateFormatVersion: '2010-09-09'
Description: Generic Compute Web Tier Launcher

Mappings:
  BaseAmi:
    us-west-2:
      ami: ami-79873901 # Ubuntu Server 16.04 LTS (HVM)

Parameters:
  paramBastionSecurityGroup:
    Type: AWS::EC2::SecurityGroup::Id

  paramEnvironment:
    Default: dev
    Type: String

  paramInstanceType:
    Type: String

  paramKeyPairName:
    Type: AWS::EC2::KeyPair::KeyName

  paramPrivateSubnetGroupIdList:
    Type: List<AWS::EC2::Subnet::Id>

  paramPublicSubnetGroupIdList:
    Type: List<AWS::EC2::Subnet::Id>

  paramRoute53HostedZone:
    Type: AWS::Route53::HostedZone::Id

  paramRoute53Record:
    Type: String

# Maybe when I get to play with LetsEncrypt
  paramServerCertName:
    Type: String

  paramVPCId:
    Type: AWS::EC2::VPC::Id

  paramIpWhitelist:
    Type: String

  paramAssignPublicIpCompute:
    Type: String

  paramDokkuPublicKey:
    Type: String

  paramJenkiPublicKey:
    Type: String

  paramComponent:
    Type: String

  paramJenkinsSecurityGroup:
    Type: AWS::EC2::SecurityGroup::Id

  paramIpWhitelistA:
    Type: String

  paramIpWhitelistB:
    Type: String

Resources:
  asgGenericCompute:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      Cooldown: 300
      DesiredCapacity: 1
      HealthCheckGracePeriod: 300
      HealthCheckType: EC2
      LaunchConfigurationName: !Ref lcGenericCompute
      LoadBalancerNames:
      - !Ref elbGenericCompute
      MaxSize: 1
      MinSize: 1
      Tags:
      - Key: Name
        Value: !Sub autoscale-${paramComponent}
        PropagateAtLaunch: true
      - Key: environment
        Value: !Ref paramEnvironment
        PropagateAtLaunch: true
      - Key: applicaton
        Value: generic-compute
        PropagateAtLaunch: true
      - Key: contractor
        Value: cloudformation-autoscaling
        PropagateAtLaunch: true
      TerminationPolicies:
      - Default
      VPCZoneIdentifier: !Ref paramPrivateSubnetGroupIdList

  dnsGenericComputeElb:
    Type: AWS::IAM::InstanceProfile
    Properties:
      AliasTarget:
        DNSName:
          Fn::GetAtt:
          - elbGenericCompute
          - DNSName
        HostedZoneId:
          Fn::GetAtt:
          - elbGenericCompute
          - CanonicalHostedZoneNameID
      HostedZoneId: !Ref paramRoute53HostedZone
      Name: !Ref paramRoute53Record
      Type: A
    Type: AWS::Route53::RecordSet

  dnsGenericComputeElbWildcard:
    Type: AWS::IAM::InstanceProfile
    Properties:
      AliasTarget:
        DNSName:
          Fn::GetAtt:
          - elbGenericCompute
          - DNSName
        HostedZoneId:
          Fn::GetAtt:
          - elbGenericCompute
          - CanonicalHostedZoneNameID
      HostedZoneId: !Ref paramRoute53HostedZone
      Name: !Sub
        - '*.${baseDomain}'
        - { baseDomain: !Ref paramRoute53Record }
      Type: A
    Type: AWS::Route53::RecordSet

  ec2GenericComputeInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles:
      - !Ref ec2GenericComputeRole

  ec2GenericComputePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyDocument:
        Statement:
        - Action:
          - ec2:DescribeInstances
          - ec2:DescribeTags
          - autoscaling:DescribeAutoScalingInstances
          Effect: Allow
          Resource: '*'
        Version: '2012-10-17'
      PolicyName: !Sub generic-compute-cluster-policy-${paramComponent}
      Roles:
      - !Ref ec2GenericComputeRole

  ec2GenericComputeRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Action:
          - sts:AssumeRole
          Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
        Version: '2012-10-17'
      Path: /

  elbGenericCompute:
    Type: AWS::ElasticLoadBalancing::LoadBalancer
    Properties:
      ConnectionDrainingPolicy:
        Enabled: true
        Timeout: 300
      ConnectionSettings:
        IdleTimeout: 60
      CrossZone: true
      HealthCheck:
        HealthyThreshold: 3
        Interval: 10
        Target: TCP:22
        Timeout: 5
        UnhealthyThreshold: 3
      LoadBalancerName: !Sub elb-${paramComponent}
      Listeners:
      - InstancePort: 80
        InstanceProtocol: HTTP
        LoadBalancerPort: 80
        Protocol: HTTP
      - InstancePort: 22
        InstanceProtocol: TCP
        LoadBalancerPort: 22
        Protocol: TCP
      Scheme: internet-facing
      SecurityGroups:
      - !Ref sgGenericComputeElb
      Subnets: !Ref paramPublicSubnetGroupIdList

  lcGenericCompute:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      AssociatePublicIpAddress: !Ref paramAssignPublicIpCompute
      BlockDeviceMappings:
      - DeviceName: /dev/xvdb
        Ebs:
          DeleteOnTermination: true
          VolumeSize: 16
          VolumeType: gp2
      IamInstanceProfile: !Ref ec2GenericComputeInstanceProfile
      ImageId:
        Fn::FindInMap:
        - BaseAmi
        - !Ref AWS::Region
        - ami
      InstanceType: !Ref paramInstanceType
      KeyName: !Ref paramKeyPairName
      SecurityGroups:
      - !Ref sgGenericComputeEc2
      UserData:
        Fn::Base64: !Sub |
          #!/bin/bash -v
          hostname --ip-address;
          wget https://raw.githubusercontent.com/dokku/dokku/v0.11.5/bootstrap.sh
          DOKKU_TAG=v0.11.5 bash bootstrap.sh
          sleep 10
          echo "${paramDokkuPublicKey}"  | dokku ssh-keys:add KEY_NAME
          echo "${paramJenkiPublicKey}"  | dokku ssh-keys:add KEY_NAME
          systemctl stop dokku-installer && systemctl disable dokku-installer
          dokku domains:add-global ${paramRoute53Record}

  sgGenericComputeEc2:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: !Sub sgGenericComputeEc2-${paramComponent}
      GroupDescription: Security group for generic web-tier compute reources
      SecurityGroupEgress:
      - CidrIp: 0.0.0.0/0
        IpProtocol: -1
      SecurityGroupIngress:
      - FromPort: 80
        ToPort: 80
        IpProtocol: tcp
        SourceSecurityGroupId: !Ref sgGenericComputeElb
      # Yes...I am punching a whole on a load balancer on an SSH port...all kinds of wrong
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        SourceSecurityGroupId: !Ref sgGenericComputeElb
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        SourceSecurityGroupId: !Ref paramBastionSecurityGroup
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        SourceSecurityGroupId: !Ref paramJenkinsSecurityGroup
      Tags:
      - Key: Name
        Value: !Sub sgGenericComputeEc2-${paramComponent}
      VpcId:
        Ref: paramVPCId

  sgGenericComputeEc2Ingress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref sgGenericComputeEc2
      IpProtocol: -1
      SourceSecurityGroupId: !Ref sgGenericComputeEc2

  sgGenericComputeElb:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: !Sub sgGenericComputeElb-${paramComponent}
      GroupDescription: Security group for generic web-tier load balancer resources
      SecurityGroupEgress:
      - CidrIp: 0.0.0.0/0
        IpProtocol: -1
      SecurityGroupIngress:
      - FromPort: 80
        ToPort: 80
        IpProtocol: tcp
        CidrIp: !Ref paramIpWhitelist
      # Yes...I am punching a whole on a load balancer on an SSH port...all kinds of wrong
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        CidrIp: !Ref paramIpWhitelist
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        SourceSecurityGroupId: !Ref paramJenkinsSecurityGroup
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        CidrIp: !Ref paramIpWhitelistA
      - FromPort: 22
        ToPort: 22
        IpProtocol: tcp
        CidrIp: !Ref paramIpWhitelistB
      Tags:
      - Key: Name
        Value: !Sub sgGenericComputeElb-${paramComponent}
      VpcId: !Ref paramVPCId

Outputs:
  outEC2SG:
    Value: !Ref sgGenericComputeEc2
  
  outRoute53DomainName:
    Value: !Ref dnsGenericComputeElb